# U5_3-3E_I-6_ArduinoApplication

<h2>Certain university experiment No.I-6 Arduino Applicsation</h2>

実験No.I-6<br>
学生実験で作成したArduinoスケッチ晒し．<br>
某大学の学生はレポート楽になりますね〜w


<h3>バージョン情報</h3>
<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/U5_3-3E_I-6_ArduinoApplication/tree/ver_1.0.0">ver_1.0.0</a></h4>
実験前の予習．「たぶん動くだろう」という恐ろしい考えでテキトーに描きました．<br>
ファイル内のex1〜problem5までは動作確認済み．<br>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/U5_3-3E_I-6_ArduinoApplication/tree/ver_1.0.1">ver_1.0.1</a></h4>
「.DS_store」削除版です．<br>
その他中身は変わりありません．<br>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/U5_3-3E_I-6_ArduinoApplication/tree/ver_2.0.0">ver_2.0.0</a></h4>
「problemAddEx6_LCD」，「problemAddEx6_decCounter」，「vr_R」以外動作確認済み．<br>
<ul>
    <li>
        スケッチのバグの修正
    </li>
    <li>
        コメント文の修正と追記
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/U5_3-3E_I-6_ArduinoApplication/tree/ver_2.1.0">ver_2.1.0</a></h4>
<ul>
    <li>
        スケッチの最適化
    </li>
    <li>
        コメント文の修正
    </li>
</ul>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/U5_3-3E_I-6_ArduinoApplication/tree/ver_2.1.1">ver_2.1.1</a></h4>
「.DS_store」削除版です．<br>
その他ファイル内容に変更ありません．<br>

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/U5_3-3E_I-6_ArduinoApplication/tree/ver_2.1.2">ver_2.1.2</a></h4>
マージ不能のため新ブランチ作成

<h4><a href="https://github.com/UtsusemiUltimate-of-the-darkness/U5_3-3E_I-6_ArduinoApplication/tree/ver_2.2.0">ver_2.2.0</a></h4>
ドレミの歌が正常に再生されないバグを修正
